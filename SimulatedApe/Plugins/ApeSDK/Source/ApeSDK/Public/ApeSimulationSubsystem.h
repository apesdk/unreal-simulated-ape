﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ApeSDKTypes.h"
#include "Subsystems/WorldSubsystem.h"
#include "ApeSimulationSubsystem.generated.h"


/**
 * 
 */
UCLASS()
class APESDK_API UApeSimulationSubsystem : public UTickableWorldSubsystem
{
	GENERATED_BODY()

public:

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	void SimInit(uint64 Randomize, uint64 OffScreenSize, uint64 LandBufferSize);

	void SimEnd();
	
	virtual void Tick(float Delta) override;

	virtual TStatId GetStatId() const override;

	UFUNCTION(BlueprintPure)
	FString GetOutputString() const;

	uint16* GetLandTopography() const;
	
	uint8* GetDraw(int32 View, int32 Width, int32 Height);

	FColor GetColorByIndex(uint8 Index) const;

	uint128 GetBeingListCurrentSize() const;

	uint128 GetBeingListMaxSize() const;

	bool GetBeing(uint8 Index, FBeingWrapper* Being) const;

	uint128 SelectBeingFromMouse(FVector2D Position) const;

	void SelectBeingFromKeyboard(bool bMoveForward) const;
protected:

	TWeakPtr<FApeSDKDLLWrapper> ApeSDKWrapper;

	uint8 Palette[256 * 3];
};
