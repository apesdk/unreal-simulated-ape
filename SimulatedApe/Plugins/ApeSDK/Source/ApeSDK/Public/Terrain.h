﻿#pragma once

#include "CoreMinimal.h"
#include "ApeSDKTypes.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Terrain.generated.h"

namespace GeometryStatics
{
	APESDK_API uint128 GetSingleCoordForPoint(uint32 X, uint32 Y);
 
	APESDK_API bool GetApePointForCoord(uint32 X, uint32 Y, uint32& FinalX, uint32& FinalY);

	APESDK_API uint32 GetSectionIndexFromPoint(uint32 PointX, uint32 PointY);
}

UCLASS()
class APESDK_API ATerrain : public AActor
{
	GENERATED_BODY()

public:

	ATerrain(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	
	FVector GetTerrainPointFromCoord(uint32 PointX, uint32 PointY) const;

	bool IsInsideRenderedSection(uint32 PointX, uint32 PointY);
	
	void SetActiveSectionFromPoint(uint32 PointX, uint32 PointY);

protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Terrain")
	UProceduralMeshComponent* ProceduralMesh;

	TArray<int32> ActiveSections;

	void SetSectionMooreNeighborhood(int32 NewSection);

private:
	void InitializeTriangleBuffer(int32 SizeX, int32 SizeY, TArray<int32>& Triangles) const;
};
