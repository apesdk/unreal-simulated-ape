﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ApeSimulationMainWidget.generated.h"


class UApeSimulationSubsystem;
class UBackgroundBlur;
class UBorder;
class UCanvasPanel;
class UImage;
class UTextBlock;
class UTexture2D;

#define APE_SIMULATION_USE_TERRAIN 0

/**
 * 
 */
UCLASS()
class APESDK_API UApeSimulationMainWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual bool Initialize() override;

	virtual void NativePreConstruct() override;

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:

	void RenderImage(UApeSimulationSubsystem* ApeSimulationSubsystem, UTexture2D* Texture, int32 ViewType, int32 MapWidth, int32 MapHeight);

	UFUNCTION()
	FEventReply OnMapMouseDown(const FGeometry& Geometry, const FPointerEvent& MouseEvent);

	UPROPERTY(Transient)
	UTexture2D* MapTexture;
	
	UPROPERTY(Transient)
	UTexture2D* TerrainTexture;
	
	UPROPERTY()
	UBackgroundBlur* Blur;

	UPROPERTY()
	UBorder* Border;

	UPROPERTY()
	UTextBlock* SimulationText;

	UPROPERTY()
	UImage* Terrain;

	UPROPERTY()
	UImage* Map;
};
