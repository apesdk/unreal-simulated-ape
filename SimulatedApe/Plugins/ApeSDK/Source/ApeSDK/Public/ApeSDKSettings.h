
#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"

#include "ApeSDKSettings.generated.h"

UCLASS(config = Game, defaultconfig)
class APESDK_API UApeSDKSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:

	UPROPERTY(config, EditAnywhere, Category = "Terrain", Meta = (UIMin = "1", ClamMin = "1"))
	uint32 MapBitSize = 9;

	UPROPERTY(VisibleAnywhere, Category = "Terrain")
	uint32 MapSize = 1 << MapBitSize;

	UPROPERTY(config, EditAnywhere, Category = "Terrain", Meta = (UIMin = "1", ClamMin = "1"))
	uint32 MapHiResBitSize = MapBitSize + 3;

	UPROPERTY(VisibleAnywhere, Category = "Terrain")
	uint32 MapHiResSize = 1 << (MapBitSize + 3);

	UPROPERTY(config, EditAnywhere, Category = "Terrain", Meta = (UIMin = "1", ClamMin = "1"))
	uint32 SectionAmount = 16u;

	UPROPERTY(config, EditAnywhere, Category = "Terrain", Meta = (UIMin = "0.1", ClamMin = "0.1"))
	float DistanceBetweenPixels = 100.f;

	UPROPERTY(config, EditAnywhere, Category = "Terrain")
	TSoftObjectPtr<UMaterial> TerrainMaterial;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& Event) override
	{
		if (Event.GetPropertyName() == GET_MEMBER_NAME_CHECKED(UApeSDKSettings, MapBitSize))
		{
			MapSize = 1 << MapBitSize;
			MapHiResBitSize = MapSize + 3;
			MapHiResSize = 1 << MapHiResBitSize;
		}
		else if (Event.GetPropertyName() == GET_MEMBER_NAME_CHECKED(UApeSDKSettings, SectionAmount))
		{
			SectionAmount = FMath::RoundUpToPowerOfTwo(SectionAmount);
		}
	}
#endif	//WITH_EDITOR
private:

	
};
