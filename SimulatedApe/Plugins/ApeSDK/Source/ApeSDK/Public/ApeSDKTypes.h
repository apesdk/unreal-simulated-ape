﻿#pragma once


#define    NUM_VIEW    (0)
#define    NUM_TERRAIN (1)
#define    NUM_CONTROL (2)
#define    NUM_NIL     (3)

enum ESharedCycleState : uint8;
struct FBeingWrapper;

using String = char*;

using int128 = long long;
using uint128 = unsigned long long;

using SimInitPtr = void(*)(uint128, uint128, uint128);				//apesdk_init

using SimClosePtr = void(*)();	                                    //apesdk_close

using SimCyclePtr = ESharedCycleState(*)(uint128);					//apesdk_cycle

using SimDrawPtr = uint8* (*)(int128, int128, int128);				//apesdk_draw

using SimGetOutputStringPtr = String(*)();	                        //apesdk_get_output_string

using SimGetLandTopographyPtr = uint16* (*)();						//apesdk_get_land_topography

using SimGetColorPalettePtr = void (*)(uint8*);						//apesdk_get_color_palette

using SimGetBeingsListSizePtr = uint128 (*)();						//apesdk_get_being_list_max_size apesdk_get_being_list_current_size

using SimGetBeingPtr = uint8 (*)(uint8, FBeingWrapper*);			//apesdk_get_being

using SimSelectBeingPtr = uint128 (*)(uint128, uint128);			//apesdk_select_being_from_mouse

using SimChangeSelectedPtr = void (*)(uint8);						//apesdk_change_selected


enum ESharedCycleState : uint8
{
	SHARED_CYCLE_OK = 0,
	SHARED_CYCLE_QUIT,
	SHARED_CYCLE_DEBUG_OUTPUT,
	SHARED_CYCLE_NEW_APES
};

union FVect2
{
	struct
	{
		int128 X;
		int128 Y;
	};
	int128 Data[2];
};

struct FBeingWrapper
{
	FVect2 Location;
	uint8 bIsSelected : 1;
	uint16 FacingAngle;	//Degrees
};

struct FApeSDKDLLWrapper
{
public:
	FApeSDKDLLWrapper(const FString& Path);
	
	~FApeSDKDLLWrapper();
	
	FORCEINLINE void SimInit(uint64 Randomize, uint64 OffScreenSize, uint64 LandBufferSize) const
	{
		if(SimInitFunc)
		{
			SimInitFunc(Randomize, OffScreenSize, LandBufferSize);
		}
	}

	FORCEINLINE void SimClose() const
	{
		if(SimCloseFunc)
		{
			SimCloseFunc();
		}
	}

	FORCEINLINE void SimCycle(const UWorld* World) const
	{
		if(World && SimCycleFunc)
		{
			const uint128 Time = World->TimeSeconds;
			SimCycleFunc(Time);
		}
	}

	FORCEINLINE uint8* SimDraw(int32 View, int32 Width, int32 Height) const
	{
		if (SimDrawFunc)
		{
			return SimDrawFunc(View, Width, Height);
		}

		return nullptr;
	}

	FORCEINLINE FString SimGetOutputString() const
	{
		if(SimGetOutputStringFunc)
		{
			if(const String Output = SimGetOutputStringFunc())
			{
				return FString(Output);
			}
		}

		return FString();
	}

	FORCEINLINE uint16* SimGetLandTopography() const
	{
		if (SimGetLandTopographyFunc)
		{
			return SimGetLandTopographyFunc();
		}

		return nullptr;
	}

	FORCEINLINE void SimGetColorPalette(uint8* Palette) const
	{
		if (SimGetColorPaletteFunc)
		{
			SimGetColorPaletteFunc(Palette);
		}
	}

	FORCEINLINE uint128 SimGetBeingListCurrentSize() const
	{
		if(SimGetBeingListCurrentSizeFunc)
		{
			return SimGetBeingListCurrentSizeFunc();
		}

		return 0;
	}

	FORCEINLINE uint128 SimGetBeingListMaxSize() const
	{
		if(SimGetBeingListMaxSizeFunc)
		{
			return SimGetBeingListMaxSizeFunc();
		}

		return 0;
	}

	FORCEINLINE bool SimGetBeing(uint8 Index, FBeingWrapper* Being) const
	{
		if(SimGetBeingFunc)
		{
			return SimGetBeingFunc(Index, Being) > 0;
		}
		
		return false;
	}

	FORCEINLINE uint128 SimSelectBeingFromMouse(FVector2D Position) const
	{
		if(SimSelectBeingFunc)
		{
			SimSelectBeingFunc(static_cast<uint128>(Position.X), static_cast<uint128>(Position.Y));
		}

		return 0UL;
	}

	FORCEINLINE void SimChangeSelected(bool bIsMovingForward) const
	{
		if(SimChangeSelectedFunc)
		{
			SimChangeSelectedFunc(bIsMovingForward);
		}
	}
	
private:
	bool TryLoadSDKDLL(const FString& Path, bool bIsEnginePlugin);
	
	void* ApeSDKDLLHandle = nullptr;

	SimInitPtr SimInitFunc = nullptr;
	SimClosePtr SimCloseFunc = nullptr;

	SimCyclePtr SimCycleFunc = nullptr;

	SimDrawPtr SimDrawFunc = nullptr;

	SimGetOutputStringPtr SimGetOutputStringFunc = nullptr;

	SimGetLandTopographyPtr SimGetLandTopographyFunc = nullptr;

	SimGetColorPalettePtr SimGetColorPaletteFunc = nullptr;

	SimGetBeingsListSizePtr SimGetBeingListCurrentSizeFunc = nullptr;
	
	SimGetBeingsListSizePtr SimGetBeingListMaxSizeFunc = nullptr;

	SimGetBeingPtr SimGetBeingFunc = nullptr;

	SimSelectBeingPtr SimSelectBeingFunc = nullptr;
	
	SimChangeSelectedPtr SimChangeSelectedFunc = nullptr;
};