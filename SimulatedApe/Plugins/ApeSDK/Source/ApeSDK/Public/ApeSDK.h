// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ApeSDKTypes.h"
#include "Modules/ModuleManager.h"


class FApeSDKModule : public IModuleInterface
{
public:

	static FApeSDKModule& Get();

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	FORCEINLINE TWeakPtr<FApeSDKDLLWrapper> GetApeSDKWrapper() const { return ApeSDKWrapper; }

	
protected:

	TSharedPtr<FApeSDKDLLWrapper> ApeSDKWrapper;
};
