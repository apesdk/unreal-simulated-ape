// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class ApeSDK : ModuleRules
{
	public ApeSDK(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"UMG",
                "ProceduralMeshComponent",
                "DeveloperSettings"
            }
		);
			
		
		PrivateDependencyModuleNames.AddRange(
	new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
			}
		);

		RuntimeDependencies.Add(Path.Combine(PluginDirectory, "Binaries/Win64/apesdk.dll"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "ThirdParty/ApeSDK/Include"));
	}
}
