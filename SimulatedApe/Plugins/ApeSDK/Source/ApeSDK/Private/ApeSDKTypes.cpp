﻿#include "ApeSDKTypes.h"

FApeSDKDLLWrapper::FApeSDKDLLWrapper(const FString& Path)
{
	const bool bLoadedDLL = TryLoadSDKDLL(Path, false) || TryLoadSDKDLL(Path, true);
	check(bLoadedDLL);

	SimInitFunc = static_cast<SimInitPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_init")));
	SimCloseFunc = static_cast<SimClosePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_close")));
	SimCycleFunc =  static_cast<SimCyclePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_cycle")));
	SimDrawFunc = static_cast<SimDrawPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_draw")));
	SimGetOutputStringFunc = static_cast<SimGetOutputStringPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_get_output_string")));
	SimGetLandTopographyFunc = static_cast<SimGetLandTopographyPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_get_land_topography")));
	SimGetColorPaletteFunc = static_cast<SimGetColorPalettePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_get_color_palette")));
	SimGetBeingListCurrentSizeFunc = static_cast<SimGetBeingsListSizePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_get_being_list_current_size")));
	SimGetBeingListMaxSizeFunc = static_cast<SimGetBeingsListSizePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_get_being_list_max_size")));
	SimGetBeingFunc = static_cast<SimGetBeingPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_get_being")));
	SimSelectBeingFunc = static_cast<SimSelectBeingPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_select_being_from_mouse")));
	SimChangeSelectedFunc = static_cast<SimChangeSelectedPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("apesdk_change_selected")));
}

FApeSDKDLLWrapper::~FApeSDKDLLWrapper()
{
	if(ApeSDKDLLHandle)
	{
		FPlatformProcess::FreeDllHandle(ApeSDKDLLHandle);
	}
}

bool FApeSDKDLLWrapper::TryLoadSDKDLL(const FString& Path, bool bIsEnginePlugin)
{
	const FString FilePath = FPaths::Combine(bIsEnginePlugin ? FPaths::EnginePluginsDir() : FPaths::ProjectPluginsDir(), Path);
	ApeSDKDLLHandle = FPlatformProcess::GetDllHandle(*FilePath);

	return ApeSDKDLLHandle != nullptr;
}