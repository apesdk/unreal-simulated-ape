﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ApeSimulationSubsystem.h"

#include "ApeSDK.h"
#include "ApeSDKTypes.h"

void UApeSimulationSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	ApeSDKWrapper = FApeSDKModule::Get().GetApeSDKWrapper();
}

void UApeSimulationSubsystem::SimInit(uint64 Randomize, uint64 OffScreenSize, uint64 LandBufferSize)
{
	ApeSDKWrapper.Pin()->SimInit(Randomize, OffScreenSize, LandBufferSize);
	ApeSDKWrapper.Pin()->SimGetColorPalette(Palette);
}

void UApeSimulationSubsystem::SimEnd()
{
	ApeSDKWrapper.Pin()->SimClose();
}

FString UApeSimulationSubsystem::GetOutputString() const
{
	return ApeSDKWrapper.Pin()->SimGetOutputString();
}

uint16* UApeSimulationSubsystem::GetLandTopography() const
{
	return ApeSDKWrapper.Pin()->SimGetLandTopography();
}

uint8* UApeSimulationSubsystem::GetDraw(int32 View, int32 Width, int32 Height)
{
	ApeSDKWrapper.Pin()->SimGetColorPalette(Palette);
	return ApeSDKWrapper.Pin()->SimDraw(View, Width, Height);
	
}

FColor UApeSimulationSubsystem::GetColorByIndex(uint8 Index) const
{
	const uint32 IndexTransformed = Index * 3;
	return FColor(Palette[IndexTransformed], Palette[IndexTransformed + 1], Palette[IndexTransformed + 2]);
}

uint128 UApeSimulationSubsystem::GetBeingListCurrentSize() const
{
	return ApeSDKWrapper.Pin()->SimGetBeingListCurrentSize();
}

uint128 UApeSimulationSubsystem::GetBeingListMaxSize() const
{
	return ApeSDKWrapper.Pin()->SimGetBeingListMaxSize();
}

bool UApeSimulationSubsystem::GetBeing(uint8 Index, FBeingWrapper* Being) const
{
	return ApeSDKWrapper.Pin()->SimGetBeing(Index, Being);
}

uint128 UApeSimulationSubsystem::SelectBeingFromMouse(FVector2D Position) const
{
	return ApeSDKWrapper.Pin()->SimSelectBeingFromMouse(Position);
}

void UApeSimulationSubsystem::SelectBeingFromKeyboard(bool bMoveForward) const
{
	ApeSDKWrapper.Pin()->SimChangeSelected(bMoveForward);
}

void UApeSimulationSubsystem::Tick(float Delta)
{
	Super::Tick(Delta);
	ApeSDKWrapper.Pin()->SimCycle(GetWorld());
}

TStatId UApeSimulationSubsystem::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(UApeSimulationSubsystem, STATGROUP_Tickables);
}