// Copyright Epic Games, Inc. All Rights Reserved.

#include "ApeSDK.h"


#define LOCTEXT_NAMESPACE "FApeSDKModule"

void FApeSDKModule::StartupModule()
{
#if WITH_EDITOR
	FString Path = FPaths::Combine("ApeSDK/Source/ApeSDK/ThirdParty/Binaries", "apesdk.dll");
#else
	FString Path = FPaths::Combine(FPaths::RootDir(), "apesdk.dll");
#endif	//WITH_EDITOR
	
	ApeSDKWrapper = MakeShareable(new FApeSDKDLLWrapper(Path));
}

void FApeSDKModule::ShutdownModule()
{
	ApeSDKWrapper.Reset();
}

FApeSDKModule& FApeSDKModule::Get()
{
	return FModuleManager::LoadModuleChecked<FApeSDKModule>(TEXT("ApeSDK"));
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FApeSDKModule, ApeSDK)