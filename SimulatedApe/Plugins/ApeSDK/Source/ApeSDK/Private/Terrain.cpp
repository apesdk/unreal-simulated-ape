﻿#include "Terrain.h"

#include "ApeSDKSettings.h"
#include "ApeSimulationSubsystem.h"
#include "ApeSDKTypes.h"

#define POS_HIRES(Point, Settings) Point + (Settings->MapHiResSize * 2) & (Settings->MapHiResSize - 1)

enum ESmallSectionSide
{
	Horizontal	= 0b01,
	Vertical	= 0b10
};

uint128 GeometryStatics::GetSingleCoordForPoint(uint32 X, uint32 Y)
{
 	if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
 	{
 		const uint128 NewX = POS_HIRES(X, Settings);
 		uint128 NewY = POS_HIRES(Y, Settings);
 		NewY <<= Settings->MapHiResBitSize;

 		return NewX | NewY;
 	}

 	return 0;
}

bool GeometryStatics::GetApePointForCoord(uint32 X, uint32 Y, uint32& FinalX, uint32& FinalY)
{
 	if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
 	{
 		const uint32 HalfHiResSize = Settings->MapHiResSize / 2;
 		
 		const uint32 NewX = POS_HIRES(HalfHiResSize, Settings);
 		const uint32 NewY = POS_HIRES(HalfHiResSize, Settings);

 		FinalX = NewX + X;
 		FinalY = NewY + Y;
 		
 		return true;
 	}

 	return false;
}

uint32 GeometryStatics::GetSectionIndexFromPoint(uint32 PointX, uint32 PointY)
{
	uint32 SectionSize = 1;
	uint32 Sections = 1;

	if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
	{
		Sections = Settings->SectionAmount;
		SectionSize = Settings->MapHiResSize / Sections;
	}

	const uint32 NewPointX = PointX / SectionSize;
	const uint32 NewPointY = PointY / SectionSize;

	return NewPointY * Sections + NewPointX;
}

ATerrain::ATerrain(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ProceduralMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("TerrainMesh"));

	RootComponent = ProceduralMesh;
	ProceduralMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	PrimaryActorTick.bCanEverTick = false;
}

void ATerrain::BeginPlay()
{
	Super::BeginPlay();

	check(ProceduralMesh);

	const UWorld* World = GetWorld();
	check(World);

	const UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>();
	check(ApeSimulationSubsystem);
	
	if (const uint16* Topology = ApeSimulationSubsystem->GetLandTopography())
	{
		UMaterial* Material = nullptr;
		int32 SectionSize = 1;
		int32 Sections = 1;
		float DistanceBetweenPixels = 0;
		
		if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
		{
			Sections = Settings->SectionAmount;
			SectionSize = Settings->MapHiResSize / Sections;
			
			DistanceBetweenPixels = Settings->DistanceBetweenPixels;
			Material = Settings->TerrainMaterial.LoadSynchronous();
		}

		FCriticalSection Mutex;
		ProceduralMesh->ClearAllMeshSections();

		UProceduralMeshComponent* MeshCapture = ProceduralMesh;
		//MeshCapture->bUseAsyncCooking = true;

		TArray<int32> TriangleList[4];

		InitializeTriangleBuffer(SectionSize + 1, SectionSize + 1, TriangleList[0]);
		InitializeTriangleBuffer(SectionSize, SectionSize + 1, TriangleList[1]);
		InitializeTriangleBuffer(SectionSize + 1, SectionSize, TriangleList[2]);
		InitializeTriangleBuffer(SectionSize, SectionSize, TriangleList[3]);

		ParallelFor(Sections * Sections, [&ApeSimulationSubsystem, &MeshCapture, &Topology, &TriangleList, &Material, &Mutex, Sections, SectionSize, DistanceBetweenPixels](int32 Section)
        {
        	const uint32 RowIndex = Section / Sections;
			const uint32 ColumnIndex = Section % Sections;

        	const uint8 FinalSize = (ColumnIndex + 1 == Sections) | ((RowIndex + 1 == Sections) << 1);

        	const int32 AdjustSizeX = FinalSize & ESmallSectionSide::Horizontal ? SectionSize : SectionSize + 1;
        	const int32 AdjustSizeY = FinalSize & ESmallSectionSide::Vertical ? SectionSize : SectionSize + 1;

        	TArray<FVector> Vertices;
        	Vertices.Reserve(AdjustSizeY * AdjustSizeX);
        	
        	TArray<FLinearColor> VertexColors;
        	VertexColors.Reserve(AdjustSizeY * AdjustSizeX);
        	
			for (int32 i = 0; i < AdjustSizeY; ++i)
			{
				for (int32 j = 0; j < AdjustSizeX; ++j)
				{
					uint16 TopologyCell;
					FVector Vertex;
					
					if(j > SectionSize && i > SectionSize)
					{
						TopologyCell = Topology[GeometryStatics::GetSingleCoordForPoint((ColumnIndex + 1) * SectionSize, (RowIndex + 1) * SectionSize)];
						Vertex = FVector(((RowIndex + 1) * SectionSize) * DistanceBetweenPixels, ((ColumnIndex + 1) * SectionSize) * DistanceBetweenPixels, 0.f);
					}
					if(j > SectionSize)
					{
						TopologyCell = Topology[GeometryStatics::GetSingleCoordForPoint((ColumnIndex + 1) * SectionSize, i + RowIndex * SectionSize)];
						Vertex = FVector((i + RowIndex * SectionSize) * DistanceBetweenPixels, ((ColumnIndex + 1) * SectionSize) * DistanceBetweenPixels, 0.f);
					}
					else if(i > SectionSize)
					{
						TopologyCell = Topology[GeometryStatics::GetSingleCoordForPoint(j + ColumnIndex * SectionSize, (RowIndex + 1) * SectionSize)];
						Vertex = FVector(((RowIndex + 1) * SectionSize) * DistanceBetweenPixels, (j + ColumnIndex * SectionSize) * DistanceBetweenPixels, 0.f);
					}
					else
					{
						TopologyCell = Topology[GeometryStatics::GetSingleCoordForPoint(j + ColumnIndex * SectionSize, i + RowIndex * SectionSize)];
						Vertex = FVector((i + RowIndex * SectionSize) * DistanceBetweenPixels, (j + ColumnIndex * SectionSize) * DistanceBetweenPixels, 0.f);
					}

					Vertex.Z = (TopologyCell & 0xFF) * DistanceBetweenPixels;
					Vertices.Add(Vertex);
					
					VertexColors.Add(ApeSimulationSubsystem->GetColorByIndex(TopologyCell >> 8));
				}
			}
	        
        	{
        		FScopeLock Lock(&Mutex);
				MeshCapture->CreateMeshSection_LinearColor(Section, Vertices, TriangleList[FinalSize], TArray<FVector>(), TArray<FVector2D>(), VertexColors, TArray<FProcMeshTangent>(), false);
				MeshCapture->SetMeshSectionVisible(Section, false);
				MeshCapture->SetMaterial(Section, Material);
        	}
        });

		ProceduralMesh->MarkRenderStateDirty();
	}
}

void ATerrain::InitializeTriangleBuffer(int32 SizeX, int32 SizeY, TArray<int32>& Triangles) const
{
	Triangles.Empty(6 * (SizeY - 1) * (SizeX - 1));
	
	for (int32 i = 0; i < SizeY - 1; ++i)
	{
		for (int32 j = 0; j < SizeX - 1; ++j)
		{
			int32 BaseIndex = i * SizeX + j;
			// Left section of the rectangular face
			Triangles.Add(BaseIndex);
			Triangles.Add(BaseIndex + 1);
			Triangles.Add(BaseIndex + SizeX);

			// Right section of the rectangular face
			Triangles.Add(BaseIndex + 1);
			Triangles.Add(BaseIndex + SizeX + 1);
			Triangles.Add(BaseIndex + SizeX);
		}
	}
}

bool ATerrain::IsInsideRenderedSection(uint32 PointX, uint32 PointY)
{
	const uint32 Index = GeometryStatics::GetSectionIndexFromPoint(PointX, PointY);

	return ActiveSections.Contains(Index);
}

FVector ATerrain::GetTerrainPointFromCoord(uint32 PointX, uint32 PointY) const
{
	FVector Point = FVector::ZeroVector;

	float DistanceBetweenPixels = 0;
	if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
	{
		DistanceBetweenPixels = Settings->DistanceBetweenPixels;
	}

	const UWorld* World = GetWorld();
	check(World);

	const UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>();
	check(ApeSimulationSubsystem);

	Point.X = PointY * DistanceBetweenPixels;
	Point.Y = PointX * DistanceBetweenPixels;

	if (const uint16* Topology = ApeSimulationSubsystem->GetLandTopography())
	{
		const uint16 TopologyCell = Topology[GeometryStatics::GetSingleCoordForPoint(PointX, PointY)];
		Point.Z = (TopologyCell & 0xFF) * DistanceBetweenPixels;
	}

	return Point;
}

void ATerrain::SetActiveSectionFromPoint(uint32 PointX, uint32 PointY)
{
	SetSectionMooreNeighborhood(GeometryStatics::GetSectionIndexFromPoint(PointX, PointY));
}

void ATerrain::SetSectionMooreNeighborhood(int32 NewSection)
{
	check(ProceduralMesh);
	
	int32 SectionsSqr = 1;
	int32 Sections = 1;
			
	if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
	{
		Sections = Settings->SectionAmount;
		SectionsSqr = Sections * Sections;
	}

	check(NewSection >= 0 && NewSection < SectionsSqr);

	for(const int32 Section : ActiveSections)
	{
		ProceduralMesh->SetMeshSectionVisible(Section, false);
	}

	ActiveSections.Empty();
	
	const int32 Y = NewSection / Sections;
	const int32 X = NewSection % Sections;

	//Retrieves the Moore Neighborhood
	for(int32 i = FMath::Max(Y - 1, 0); i < FMath::Min(Y + 2, Sections); ++i)
	{
		for(int32 j = FMath::Max(X - 1, 0); j < FMath::Min(X + 2, Sections); ++j)
		{
			const int32 NewIndex = (i * Sections + j);
			
			ActiveSections.Add(NewIndex);
			ProceduralMesh->SetMeshSectionVisible(NewIndex, true);
		}
	}
}