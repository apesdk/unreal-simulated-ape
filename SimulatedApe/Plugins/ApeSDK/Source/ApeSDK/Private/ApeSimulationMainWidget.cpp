﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ApeSimulationMainWidget.h"

#include "ApeSDKTypes.h"
#include "ApeSDKSettings.h"
#include "ApeSimulationSubsystem.h"
#include "Blueprint/SlateBlueprintLibrary.h"
#include "Blueprint/WidgetTree.h"
#include "Components/BackgroundBlur.h"
#include "Components/Border.h"
#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

#define GET_RED_PIGMENT(num)	(num & 0x0000FF00) >> 8
#define GET_GREEN_PIGMENT(num)	(num & 0x00FF0000) >> 16
#define GET_BLUE_PIGMENT(num)	(num & 0xFF000000) >> 24
#define GET_ALPHA_PIGMENT(num)	255 - (num & 0x000000FF)



bool UApeSimulationMainWidget::Initialize()
{
	const bool bWasInitialized = Super::Initialize();
    
	if(!HasAnyFlags(RF_ClassDefaultObject))
	{
		//RootWidget needs to be created in Initialized
		WidgetTree->RootWidget = WidgetTree->ConstructWidget<UCanvasPanel>(UCanvasPanel::StaticClass(), "Main Canvas");
	}
        
	return bWasInitialized;
}

void UApeSimulationMainWidget::NativePreConstruct()
{
	Super::NativePreConstruct();

	if(UCanvasPanel* RootCanvas = Cast<UCanvasPanel>(WidgetTree->RootWidget))
	{
		int32 MapSize = 0;

		if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
		{
			MapSize = Settings->MapSize;
		}
#if APE_SIMULATION_USE_TERRAIN
		Terrain = WidgetTree->ConstructWidget<UImage>(UImage::StaticClass(), "Landscape");
		if(UCanvasPanelSlot* TerrainSlot = RootCanvas->AddChildToCanvas(Terrain))
		{
			TerrainSlot->SetAnchors(FAnchors(1.f, 1.f, 1.f, 1.f));
			TerrainSlot->SetOffsets(FMargin(0.f, 0.f, 0.f, 0.f));
			TerrainSlot->SetAlignment(FVector2d(1.f, 1.f));
			TerrainSlot->SetSize(FVector2d(MapSize, MapSize));
			TerrainSlot->SetZOrder(0);
		}
#endif	//APE_SIMULATION_USE_TERRAIN

		Map = WidgetTree->ConstructWidget<UImage>(UImage::StaticClass(), "Mini Map");
		if(UCanvasPanelSlot* MapSlot = RootCanvas->AddChildToCanvas(Map))
		{
			MapSlot->SetAnchors(FAnchors(1.f, 0.f, 1.f, 0.f));
			MapSlot->SetPosition(FVector2d(0.f, 0.f));
			MapSlot->SetAlignment(FVector2d(1.f, 0.f));
			MapSlot->SetSize(FVector2d(MapSize, MapSize));
			MapSlot->SetZOrder(1);
		}

		Map->OnMouseButtonDownEvent.BindUFunction(this, GET_FUNCTION_NAME_CHECKED(UApeSimulationMainWidget, OnMapMouseDown));

		Blur = WidgetTree->ConstructWidget<UBackgroundBlur>(UBackgroundBlur::StaticClass(), "Text Blur");
		if(UCanvasPanelSlot* BlurSlot = RootCanvas->AddChildToCanvas(Blur))
		{
			BlurSlot->SetAnchors(FAnchors(0.f, 0.0f, .2f, 1.f));
			BlurSlot->SetOffsets(FMargin(0.f, 0.f, 0.f, 0.f));
			BlurSlot->SetAlignment(FVector2d(0.f, 0.f));
			BlurSlot->SetZOrder(1);
		}

		Border = WidgetTree->ConstructWidget<UBorder>(UBorder::StaticClass(), "Text Image Box");
		Border->SetPadding(FMargin(0.f, 0.f, 0.f, 0.f));
		Border->SetBrushColor(FColor::FromHex(FString("0x151A23FF")));
		
		Blur->AddChild(Border);

		SimulationText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass(), "Simulation");
		Border->AddChild(SimulationText);
	}
}

void UApeSimulationMainWidget::NativeConstruct()
{
	Super::NativeConstruct();
	
	if(const UCanvasPanelSlot* MapSlot = Cast<UCanvasPanelSlot>(Map->Slot.Get()))
	{
		const FVector2d MapSize = MapSlot->GetSize();
		MapTexture = UTexture2D::CreateTransient(MapSize.X, MapSize.Y);

		Map->SetBrushFromTexture(MapTexture);
	}

#if APE_SIMULATION_USE_TERRAIN
	if (const UCanvasPanelSlot* TerrainSlot = Cast<UCanvasPanelSlot>(Terrain->Slot.Get()))
	{
		const FVector2d MapSize = TerrainSlot->GetSize();
		TerrainTexture = UTexture2D::CreateTransient(MapSize.X, MapSize.Y);

		Terrain->SetBrushFromTexture(TerrainTexture);
	}
#endif	//APE_SIMULATION_USE_TERRAIN
}

void UApeSimulationMainWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if(const UWorld* World = GetWorld())
	{
		if(UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>())
	    {
			int32 MapSize = 0;

			if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
			{
				MapSize = Settings->MapSize;
			}

	        const FString Output = ApeSimulationSubsystem->GetOutputString();
	        SimulationText->SetText(FText::FromString(Output));

			RenderImage(ApeSimulationSubsystem, MapTexture, NUM_VIEW, MapSize, MapSize);
#if APE_SIMULATION_USE_TERRAIN
			RenderImage(ApeSimulationSubsystem, TerrainTexture, NUM_TERRAIN, MapSize, MapSize);
#endif //APE_SIMULATION_USE_TERRAIN
	    }
	}
}

void UApeSimulationMainWidget::RenderImage(UApeSimulationSubsystem* ApeSimulationSubsystem, UTexture2D* Texture, int32 ViewType, int32 MapWidth, int32 MapHeight)
{
	if (const uint32* DrawPointer = reinterpret_cast<const uint32*>(ApeSimulationSubsystem->GetDraw(ViewType, MapWidth, MapHeight)))
	{
		FTexture2DMipMap* MipMap = &Texture->GetPlatformData()->Mips[0];
		const uint32 TextureWidth = MipMap->SizeX;
		const uint32 TextureHeight = MipMap->SizeY;

		FByteBulkData* ImageData = &MipMap->BulkData;
		FColor* RawImageData = static_cast<FColor*>(ImageData->Lock(LOCK_READ_WRITE));

		for (uint32 i = 0; i < TextureHeight; ++i)
		{
			for (uint32 j = 0; j < TextureWidth; ++j)
			{
				const uint32 Pixel = DrawPointer[i * TextureWidth + j];
				RawImageData[i * TextureWidth + (TextureWidth - j)] = FColor(GET_RED_PIGMENT(Pixel), GET_GREEN_PIGMENT(Pixel), GET_BLUE_PIGMENT(Pixel), GET_ALPHA_PIGMENT(Pixel));
			}
		}

		ImageData->Unlock();
		Texture->UpdateResource();
	}
}

FEventReply UApeSimulationMainWidget::OnMapMouseDown(const FGeometry& Geometry, const FPointerEvent& MouseEvent)
{
	FEventReply Reply;
	Reply.NativeReply = FReply::Unhandled();
	
	if(const UWorld* World = GetWorld())
	{
		if(UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>())
		{
			FVector2D Position = (MouseEvent.GetScreenSpacePosition() - Geometry.GetAbsolutePosition()) / Geometry.Scale;
			Position.X = FMath::RoundHalfFromZero(Position.X);
			Position.Y = FMath::RoundHalfFromZero(Position.Y);
			
			const uint128 ReplyCode = ApeSimulationSubsystem->SelectBeingFromMouse(Position);
			
			Reply.NativeReply = ReplyCode > 0 ? FReply::Handled() : Reply.NativeReply;
		}
	}
	
	return Reply;
}