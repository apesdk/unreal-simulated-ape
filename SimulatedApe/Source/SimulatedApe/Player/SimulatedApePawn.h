// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SimulatedApePawn.generated.h"

class AApe;
class UCameraComponent;
class USpringArmComponent;

/**
 * 
 */
UCLASS()
class SIMULATEDAPE_API ASimulatedApePawn : public APawn
{
	GENERATED_BODY()

public:

	ASimulatedApePawn(const FObjectInitializer& ObjectInitializer);

    virtual void Tick(float DeltaSeconds) override;

	bool TrySetSelectedApe(AApe* NewApe);

protected:
	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* CameraComponent;
	
	TWeakObjectPtr<AApe> SelectedApe;
};
