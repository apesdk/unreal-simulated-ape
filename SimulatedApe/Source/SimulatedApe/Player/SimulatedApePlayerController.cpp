// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulatedApePlayerController.h"

#include "ApeSimulationSubsystem.h"


void ASimulatedApePlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetShowMouseCursor(true);
}

void ASimulatedApePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if(InputComponent)
	{
		InputComponent->BindAction<FSelectBeingDelegate>(FName("SelectNext"), EInputEvent::IE_Pressed, this, &ASimulatedApePlayerController::SelectBeing, true);
		InputComponent->BindAction<FSelectBeingDelegate>(FName("SelectPrevious"), EInputEvent::IE_Pressed, this, &ASimulatedApePlayerController::SelectBeing, false);
	}
}

void ASimulatedApePlayerController::SelectBeing(bool bIsForward)
{
	if(const UWorld* World = GetWorld())
	{
		if(const UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>())
		{
			ApeSimulationSubsystem->SelectBeingFromKeyboard(bIsForward);
		}
	}
}