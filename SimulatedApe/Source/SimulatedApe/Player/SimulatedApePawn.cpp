// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulatedApePawn.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "SimulatedApe/Ape.h"

ASimulatedApePawn::ASimulatedApePawn(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	CameraBoom = CreateOptionalDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	RootComponent = CameraBoom;

	if(CameraBoom)
	{
		CameraBoom->TargetArmLength = 2900.0f;
		CameraBoom->SetRelativeRotation(FRotator(-45.f, 0.f, 0.f));
		
		CameraComponent = CreateOptionalDefaultSubobject<UCameraComponent>(TEXT("Camera"));

		if(CameraComponent)
		{
			CameraComponent->SetupAttachment(CameraBoom);
		}
	}
}

void ASimulatedApePawn::Tick(float DeltaSeconds)
{
	if(SelectedApe.IsValid())
	{
		SetActorLocation(SelectedApe->GetActorLocation());
	}
}

bool ASimulatedApePawn::TrySetSelectedApe(AApe* NewApe)
{
	if(NewApe && NewApe != SelectedApe)
	{
		SelectedApe = NewApe;
		return true;
	}

	return false;
}