// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SimulatedApePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIMULATEDAPE_API ASimulatedApePlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

protected:

	DECLARE_DELEGATE_OneParam(FSelectBeingDelegate, bool);
	void SelectBeing(bool bIsForward);
};
