// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimulatedApeGameMode.generated.h"

class ATerrain;
class AApe;
class AController;

/**
 * 
 */
UCLASS()
class SIMULATEDAPE_API ASimulatedApeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ASimulatedApeGameMode(const FObjectInitializer& Initializer);

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Tick(float DeltaTime) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* Exiting) override;

protected:

	TObjectPtr<ATerrain> Terrain;

	TArray<AApe*> Apes;

	TArray<AController*> RegisteredControllers;
};
