// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulatedApeGameMode.h"

#include "ApeSDKSettings.h"
#include "ApeSDKTypes.h"
#include "ApeSimulationSubsystem.h"
#include "ApeSDK/Public/Terrain.h"
#include "SimulatedApe/Ape.h"
#include "SimulatedApe/Player/SimulatedApePawn.h"
#include "SimulatedApe/Player/SimulatedApePlayerController.h"
#include "SimulatedApe/UI/SimulatedApeHUD.h"

#include "GameFramework/Controller.h"
#include "GameFramework/GameStateBase.h"
#include "Kismet/KismetMathLibrary.h"



ASimulatedApeGameMode::ASimulatedApeGameMode(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	HUDClass = ASimulatedApeHUD::StaticClass();
	DefaultPawnClass = ASimulatedApePawn::StaticClass();
	PlayerControllerClass = ASimulatedApePlayerController::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ASimulatedApeGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	if (UWorld* World = GetWorld())
	{
		if(UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>())
		{
			const FRandomStream RandomStream(FDateTime::UtcNow().GetTicks());

			int32 MapArea = 1;
			if (const UApeSDKSettings* Settings = GetDefault<UApeSDKSettings>())
			{
				MapArea = Settings->MapSize * Settings->MapSize;
			}

			ApeSimulationSubsystem->SimInit(RandomStream.GetUnsignedInt(), MapArea, 0);
		
			Terrain = World->SpawnActor<ATerrain>(ATerrain::StaticClass());
			
			
			const uint128 MaxSize = ApeSimulationSubsystem->GetBeingListMaxSize();
			for(uint128 i = 0; i < MaxSize; ++i)
			{
				AApe* Ape = World->SpawnActor<AApe>(AApe::StaticClass());
				Ape->SetHidden(true);
				Ape->SetActorEnableCollision(false);
				Ape->SetActorTickEnabled(false);
				
				Apes.Add(Ape);
			}
		}
	}
}

void ASimulatedApeGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (const UWorld* World = GetWorld())
	{
		if(UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>())
		{
			ApeSimulationSubsystem->SimEnd();
		}
	}

	for(AApe* Ape : Apes)
	{
		Ape->Destroy();
	}
	
	Apes.Empty();
}

void ASimulatedApeGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(Apes.Num() <= 0)
	{
		return;
	}

	const UWorld * World = GetWorld();
	check(World);

	const UApeSimulationSubsystem* ApeSimulationSubsystem = World->GetSubsystem<UApeSimulationSubsystem>();
	check(ApeSimulationSubsystem);

	const uint128 Size = ApeSimulationSubsystem->GetBeingListCurrentSize();
	for (uint128 i = 0; i < Size; ++i)
	{
		FBeingWrapper Being;
		if (ApeSimulationSubsystem->GetBeing(i, &Being))
		{
			if (Being.bIsSelected)
			{
				for(const AController* Controller : RegisteredControllers)
				{
					if(ASimulatedApePawn* SimulatedApePawn = Controller->GetPawn<ASimulatedApePawn>())
					{
						SimulatedApePawn->TrySetSelectedApe(Apes[i]);
					}
				}
				
				Terrain->SetActiveSectionFromPoint(Being.Location.X, Being.Location.Y);
			}
			else if(!Terrain->IsInsideRenderedSection(Being.Location.X, Being.Location.Y))
			{
				Apes[i]->SetActorHiddenInGame(true);
				Apes[i]->SetActorEnableCollision(false);
				Apes[i]->SetActorTickEnabled(false);
				continue;
			}

			Apes[i]->SetActorHiddenInGame(false);
			Apes[i]->SetActorEnableCollision(true);
			Apes[i]->SetActorTickEnabled(true);

			Apes[i]->SetActorLocation(Terrain->GetTerrainPointFromCoord(Being.Location.X, Being.Location.Y));
			Apes[i]->SetActorScale3D(FVector::OneVector + 4.f * Being.bIsSelected);
			const FRotator NewRotation = UKismetMathLibrary::RotatorFromAxisAndAngle(FVector::UpVector, Being.FacingAngle);
			
			Apes[i]->SetActorRotation(NewRotation);
		}
		else
		{
			Apes[i]->SetActorHiddenInGame(true);
			Apes[i]->SetActorEnableCollision(false);
			Apes[i]->SetActorTickEnabled(false);
		}
	}
}

void ASimulatedApeGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	RegisteredControllers.Add(NewPlayer);
}

void ASimulatedApeGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	RegisteredControllers.Remove(Exiting);
}