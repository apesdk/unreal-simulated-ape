﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SimulatedApeHUD.generated.h"

class UApeSimulationMainWidget;

/**
 * 
 */
UCLASS()
class SIMULATEDAPE_API ASimulatedApeHUD : public AHUD
{
	GENERATED_BODY()

public:

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:

	UPROPERTY()
	UApeSimulationMainWidget* MainWidget;
};
