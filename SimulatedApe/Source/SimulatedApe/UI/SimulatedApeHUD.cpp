﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulatedApeHUD.h"

#include "ApeSDK/Public/ApeSimulationMainWidget.h"
#include "Blueprint/UserWidget.h"


void ASimulatedApeHUD::BeginPlay()
{
	Super::BeginPlay();

	MainWidget = CreateWidget<UApeSimulationMainWidget>(GetOwningPlayerController(), UApeSimulationMainWidget::StaticClass());
	
	MainWidget->AddToViewport(0);
}

void ASimulatedApeHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	MainWidget->RemoveFromParent();
}