// Fill out your copyright notice in the Description page of Project Settings.


#include "Ape.h"

#include "Components/ArrowComponent.h"

AApe::AApe(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

#if WITH_EDITORONLY_DATA
    ArrowComponent = CreateEditorOnlyDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
    if (ArrowComponent)
    {
    	ArrowComponent->ArrowColor = FColor(150, 200, 255);
    	ArrowComponent->bTreatAsASprite = true;
    	ArrowComponent->SpriteInfo.Category = TEXT("Characters");
    	ArrowComponent->SpriteInfo.DisplayName = FText::FromString(TEXT("Characters"));
    	ArrowComponent->SetupAttachment(RootComponent);
    	ArrowComponent->bIsScreenSizeScaled = true;
    	ArrowComponent->SetSimulatePhysics(false);
    }
#endif // WITH_EDITORONLY_DATA
        
    Mesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
    if (Mesh)
    {
    	Mesh->AlwaysLoadOnClient = true;
    	Mesh->AlwaysLoadOnServer = false;
    	Mesh->bOwnerNoSee = false;
    	Mesh->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
    	Mesh->bCastDynamicShadow = true;
    	Mesh->bAffectDynamicIndirectLighting = true;
    	Mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
    	Mesh->SetupAttachment(RootComponent);
    	Mesh->SetCollisionEnabled( ECollisionEnabled::NoCollision);
    	Mesh->SetGenerateOverlapEvents(false);
    	Mesh->SetCanEverAffectNavigation(false);

    	const ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalMeshObject(TEXT("/Engine/EngineMeshes/SkeletalCube.SkeletalCube"));
    	Mesh->SetSkeletalMesh(SkeletalMeshObject.Object);
    }
}