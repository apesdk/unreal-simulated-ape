// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Ape.generated.h"

class UArrowComponent;

/**
 * 
 */
UCLASS()
class SIMULATEDAPE_API AApe : public APawn
{
	GENERATED_BODY()

public:

	AApe(const FObjectInitializer& Initializer);

protected:

	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* Mesh;

#if WITH_EDITORONLY_DATA
	UPROPERTY(VisibleAnywhere)
	UArrowComponent* ArrowComponent;
#endif	//WITH_EDITORONLY_DATA
};
